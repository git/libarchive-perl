include /usr/share/dpkg/default.mk

PACKAGE=libarchive-perl

# h2xs -Afn LibArchive
# h2xs -an LibArchive /usr/include/archive.h /usr/include/archive_entry.h
# perl Makefile.PL PREFIX=/usr

PPSRC = LibArchive

BUILDSRC=$(PACKAGE)-$(DEB_VERSION_UPSTREAM)
DSC=$(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION).dsc

DEB=$(PACKAGE)_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DBG_DEB=$(PACKAGE)-dbgsym_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb

all: $(DEB)

.PHONY: update-bindings
update-bindings:
	h2xs -M '^(archive|ARCHIVE)_' -v $(DEB_VERSION_UPSTREAM) -On LibArchive /usr/include/archive.h /usr/include/archive_entry.h -larchive
	echo "Please manually check and add the diff of the automatically generated update"

.PHONY: $(BUILDSRC)
$(BUILDSRC):
	rm -rf $@ $@.tmp
	cp -a $(PPSRC)/ $@.tmp
	cp -a debian $@.tmp
	perl -MDevel::PPPort -e 'Devel::PPPort::WriteFile("$@.tmp/ppport.h");'
	echo "git clone git://git.proxmox.com/git/libarchive-perl\\ngit checkout $(shell git rev-parse HEAD)" >$@.tmp/debian/SOURCE
	mv $@.tmp $@

.PHONY: dsc
dsc:
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(BUILDSRC)
	cd $(BUILDSRC); dpkg-buildpackage -S -us -uc -d

.PHONY: sbuild
sbuild: $(DSC)
	sbuild $(DSC)

.PHONY: deb
deb: $(DEB)
$(DBG_DEB): $(DEB)
$(DEB): $(PPSRC)/LibArchive.xs $(BUILDSRC)
	cd $(BUILDSRC); dpkg-buildpackage -b -us -uc
	lintian $(DEB) $(DBG_DEB)

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEB) $(DBG_DEB)
	tar cf - $(DEB) $(DBG_DEB) | ssh -X repoman@repo.proxmox.com -- upload --product pmg --dist $(UPLOAD_DIST)

.PHONY: clean
clean:
	rm -rf build $(PACKAGE)-[0-9]*/
	rm -f *.deb *.buildinfo *.build *.dsc *.changes $(PACKAGE)*tar*

.PHONY: dinstall
dinstall: $(DEB)
	dpkg -i $(DEB)
