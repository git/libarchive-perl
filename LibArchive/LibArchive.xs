#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include <archive.h>
#include <archive_entry.h>

#include "const-c.inc"

MODULE = LibArchive		PACKAGE = LibArchive		

INCLUDE: const-xs.inc

void
archive_clear_error(arg0)
	struct archive *	arg0

int
archive_filter_code(arg0, num)
	struct archive *	arg0
	int num

const char *
archive_filter_name(arg0, num)
	struct archive *	arg0
	int num

int
archive_errno(arg0)
	struct archive *	arg0

const char *
archive_error_string(arg0)
	struct archive *	arg0

int
archive_format(arg0)
	struct archive *	arg0

int
archive_read_close(arg0)
	struct archive *	arg0

ssize_t
archive_read_data(arg0, buffer, nbytes)
    PREINIT:
        SV *sv_buffer = SvROK(ST(1)) ? SvRV(ST(1)) : ST(1);
    INPUT:
	struct archive * arg0
        size_t           nbytes
        char *           buffer = sv_grow( sv_buffer, nbytes+1 );
    CLEANUP:
        if (RETVAL >= 0) {
            SvCUR_set(sv_buffer, RETVAL);
            SvPOK_only(sv_buffer);
            *SvEND(sv_buffer) = '\0';
            SvTAINTED_on(sv_buffer);
        }

int
archive_read_data_skip(arg0)
	struct archive *	arg0

int
archive_read_extract(arg0, arg1, flags)
	struct archive *	arg0
	struct archive_entry *	arg1
	int	flags

int
archive_read_free(arg0)
	struct archive *	arg0

struct archive *
archive_read_new()

int
archive_read_next_header(arg0, entry)
	struct archive *	arg0
	struct archive_entry *entry = NO_INIT
	CODE:
	RETVAL = archive_read_next_header (arg0, &entry);
	OUTPUT:
	entry
	RETVAL


int
archive_read_open_FILE(arg0, _file)
	struct archive *	arg0
	FILE *	_file

int
archive_read_open_fd(arg0, _fd, _block_size)
	struct archive *	arg0
	int	_fd
	size_t	_block_size

int
archive_read_open_filename(arg0, _filename, _block_size)
	struct archive *	arg0
	const char *	_filename
	size_t	_block_size

int
archive_read_support_filter_all(arg0)
	struct archive *	arg0

int
archive_read_support_filter_bzip2(arg0)
	struct archive *	arg0

int
archive_read_support_filter_compress(arg0)
	struct archive *	arg0

int
archive_read_support_filter_gzip(arg0)
	struct archive *	arg0

int
archive_read_support_filter_none(arg0)
	struct archive *	arg0

int
archive_read_support_filter_program(arg0, command)
	struct archive *	arg0
	const char *	command

int
archive_read_support_format_all(arg0)
	struct archive *	arg0

int
archive_read_support_format_ar(arg0)
	struct archive *	arg0

int
archive_read_support_format_cpio(arg0)
	struct archive *	arg0

int
archive_read_support_format_empty(arg0)
	struct archive *	arg0

int
archive_read_support_format_gnutar(arg0)
	struct archive *	arg0

int
archive_read_support_format_iso9660(arg0)
	struct archive *	arg0

int
archive_read_support_format_tar(arg0)
	struct archive *	arg0

int
archive_read_support_format_zip(arg0)
	struct archive *	arg0

time_t
archive_entry_atime(arg0)
	struct archive_entry *	arg0

long
archive_entry_atime_nsec(arg0)
	struct archive_entry *	arg0

time_t
archive_entry_ctime(arg0)
	struct archive_entry *	arg0

long
archive_entry_ctime_nsec(arg0)
	struct archive_entry *	arg0

mode_t
archive_entry_mode(arg0)
	struct archive_entry *	arg0

const char *
archive_entry_gname(arg0)
	struct archive_entry *	arg0

const char *
archive_entry_hardlink(arg0)
	struct archive_entry *	arg0

time_t
archive_entry_mtime(arg0)
	struct archive_entry *	arg0

long
archive_entry_mtime_nsec(arg0)
	struct archive_entry *	arg0

unsigned int
archive_entry_nlink(arg0)
	struct archive_entry *	arg0

const char *
archive_entry_pathname(arg0)
	struct archive_entry *	arg0

long
archive_entry_size(arg0)
	struct archive_entry *	arg0
	CODE:
	int64_t lsize;
	long res;
	lsize = archive_entry_size (arg0);
	if (lsize > LONG_MAX || lsize < LONG_MIN) {
		Perl_croak(aTHX_ "archive entry too large");
	}
	RETVAL = res = lsize;
	OUTPUT:
	RETVAL
	
const char *
archive_entry_symlink(arg0)
	struct archive_entry *	arg0

const char *
archive_entry_uname(arg0)
	struct archive_entry *	arg0

